# Documentation

## Documentation template

| Version |
|:--------:|
|[![Version](https://img.shields.io/badge/version-1.0.0-blue.svg)](https://img.shields.io/badge/version-1.0.0-blue.svg)|

#### Alice and Bob
Just another PlantUML example

![Alice and Bob](docs/example.png)

#### Dependencies
* [plantUML](plantuml.org)

#### LICENSE
[MIT](LICENSE)
